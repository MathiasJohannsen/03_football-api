

let urlCompetitions = 'https://api.football-data.org/v2/competitions/';

function getCompetitions() {
    fetch(urlCompetitions, {
        method: "GET",
        headers: {
            "x-auth-token": "819babcd7902454f930c154272296d78"
        }
    })
    .then(response => response.json())
    .then(function (data) {
        let html = "";
        let lastElement;
        data.competitions.forEach(element => {
            html += "<li>(" + element.id + ")(" + element.area.name + ") " + element.name + "</li>";
            console.log(element.name);
        });
        document.getElementById("list").innerHTML = html;
    })
}
getCompetitions();